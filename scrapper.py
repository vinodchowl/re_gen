#!/usr/local/bin/python2.7
# encoding: utf-8
'''
scrapper -- shortdesc

The script will fetch and parse .yaml file and get links to bugs listed
in this file and check the status of the bug (CLOSED, FIXED, OPEN)
and create a file to show RESOLVED bugs list and test case name

It defines classes_and_methods

@author:     user_name

@copyright:  2019 organization_name. All rights reserved.

@license:    license

@contact:    user_email
@deffield    updated: Updated
'''

import re
import sys
import os
import logging
import time
import http.cookiejar as cookielib
import mechanize
import yaml
import xlwt


from bs4 import BeautifulSoup
from datetime import datetime
from threading import Thread
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from queue import Queue


__all__ = []
__version__ = 0.1
__date__ = '2019-01-26'
__updated__ = '2019-01-26'

DEBUG = 0
THREAD_COUNT = 10
TEST_NAMES = {}
# Define Queue
Q = Queue(2 * THREAD_COUNT)


def get_logger_and_attach(name='main'):
    '''
    Create the logger object and attached to the console handler
    :param name: (str) Logger name
    :return : (object) logger object
    '''
    logger = logging.getLogger('CreateBrowser')
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)
    return logger


class ArgsParser:
    '''
    Argument parser class
    '''
    def __init__(self, argv=None):
        '''
        Initialization of ArgsParser
        :param argv: (sys.argv) or None
        '''
        self.log = None
        self.args_parser(argv)

    def args_parser(self, argv=None): # IGNORE:C0111
        '''Command line options.'''
    
        if argv is None:
            argv = sys.argv
        else:
            sys.argv.extend(argv)

        program_name = os.path.basename(sys.argv[0])
        program_version = "v%s" % __version__
        program_build_date = str(__updated__)
        program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
        program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
        program_license = '''%s
    
      Created by user_name on %s.

    USAGE
    ''' % (program_shortdesc, str(__date__))
    
        try:
            # Setup argument parser
            parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
            parser.add_argument("-l", "--log", dest="log", default='DEBUG', type=str, choices= ['DEBUG','WARN', 'ERROR', 'INFO'], help="log level [Default DEBUG], [Options: WARN, ERROR, INFO]")
            parser.add_argument('-V', '--version', action='version', version=program_version_message)
    
            # Process arguments
            args = parser.parse_args()
            self.log = args.log

            return 0
        except Exception as e:
            indent = len(program_name) * " "
            sys.stderr.write(program_name + ": " + repr(e) + "\n")
            sys.stderr.write(indent + "  for help use --help")
            raise(e)


class CreateBrowser():
    '''
    CreateBrowser class
    '''
    def __init__(self):
        '''
        Initialization of CreateBrowser
        '''
        self.logger = get_logger_and_attach('CreateBrowser')
        # Initialize browser.
        cj = cookielib.CookieJar()
        self.browser = mechanize.Browser()
        self.browser.set_handle_robots(False)
        self.browser.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
        self.browser.set_cookiejar(cj)

    def get_parse_content(self, url):
        """
         Get content by url 
        :param url: (str) give the url.
        """
        try:
            content = self.browser.open(url).read()
    
            # Beautiful Soup is a Python library for pulling data out of HTML files
            return BeautifulSoup(content, 'html.parser')
        except Exception as e:
            self.logger.error(f"Failed to fetch data from this url : {url}")
            self.logger.exception(e)
            return None


class MainScrapper():
    '''
    Main scrapper class, which includes all core part of scrapping
    '''
    ROW = 0
    THREADS = []
    def __init__(self, y_links):
        '''
        Initialization of the class.
        :param y_links: (list) YAML links
        '''
        self.logger = get_logger_and_attach('MainScrapper')

        self.cb = CreateBrowser()
        self.create_xls_file()

        self.logger.info("Initializing Threads")
        for _i in range(1, THREAD_COUNT):
            worker = Thread(target=self.dowork)
            worker.setDaemon(True)
            worker.start()
            self.THREADS.append(worker)
        self.logger.info("Initializing Threads completed.")
        for y_link in y_links:
            y_data = self.fetch_yaml_data(y_link)
            if y_data:
                self.get_bug_links(y_data, y_link)

    def fetch_yaml_data(self, y_link):
        '''
        returns the yaml data as a python dictionary or else None.
        :param y_link: (str) link of yaml file.
        '''
        self.logger.info(f"Fetch yaml data from link : {y_link}")
        raw_data = self.cb.get_parse_content(y_link)
        if raw_data:
            return yaml.load(raw_data.text)
        else:
            return None

    def get_bug_links(self, y_data, y_link):
        '''
        puts the bug links in to Queue, which are present in yaml file.
        :param y_data: (dict) yaml data of link
        :param y_link: link of yaml
        '''
        for project in y_data['projects']:
            for issues in project['known_issues']:
                test_names = issues.get('test_names')
                url = issues['url']
                if url:
                    TEST_NAMES['url'] = test_names
                    Q.put(url)
                else:
                    self.logger.warn(f"URL is found as 'none' in {y_link} link.")

    def get_bug_status(self, b_link):
        '''
        Open the bug and fetch the data from it.
        :param b_link: (str) Bug link
        '''
        bug_id_match = re.search('id\=(\d+)', b_link)
        if bug_id_match:
            bug_id = bug_id_match.group(1)
        else:
            self.logger.warn(f"Regex failed, to get the bug id from the link : {b_link}.")
        b_soup_data = self.cb.get_parse_content(b_link)
        b_status = b_soup_data.find('span', id='static_bug_status')
        b_status = b_status.text.strip()
        b_comment = b_soup_data.find('div', {'class': "bz_comment bz_first_comment"}).pre.text
        bug_fields = [f'Bug{bug_id}', b_status, b_comment]
        return bug_fields

    def dowork(self):
        '''
        Worker method.
        '''
        while True:
            try:
                url = Q.get()
                if url == 'kill_threads':
                    break
                self.logger.info('******************* current url:\t' + url)
                fields = self.get_bug_status(url)
                self.write_in_xls(fields)
                Q.task_done()
            except Exception as e:
                self.logger.error('******************* Exception in Dowork ************')
                self.logger.error(f'******************* url: {url}')
                self.logger.error(f'******************* Exception:{e}')
                Q.task_done()

    def create_xls_file(self):
        '''
        Create workbook with sheet1
        '''
        self.logger.info("Create workbook with sheet1.")
        self.workbook = xlwt.Workbook(encoding = 'utf-8')
        self.worksheet = self.workbook.add_sheet('sheet1')

    def write_in_xls(self, fields):
        '''
        Write in to the xls file.
        :param fields: (list) fetched bug data in the form of list.
        '''
        self.logger.info(f"write_in_xls : {fields}")
        for column, data in enumerate(fields):
            self.worksheet.write(self.ROW, column, label = data)
        self.ROW += 1

    def save_xls_file(self):
        '''
        saves the xls file with the default name report.xls
        '''
        self.workbook.save('report.xls')

    def kill_threads(self):
        '''
        Kill worker threads.
        '''
        for _i in range(THREAD_COUNT):
            Q.put('kill_threads')
        for t in self.THREADS:
            t.join()


if __name__ == "__main__":
    a_parser = ArgsParser()

    # Log file info
    PROJECT_ROOT = os.path.dirname(os.path.realpath(__name__))
    logfilename = PROJECT_ROOT + '/' + 're_gen' + '_' + datetime.now().strftime("%Y_%m_%d") + '.log'
    logging.basicConfig(
        level=getattr(logging, a_parser.log),
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        filename=logfilename,
        filemode='w'
        )

    config_data = yaml.load(open('config.yaml'))
    y_links = config_data['yaml_links']
    try:
        starttime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        # Start scraping the forum
        logging.info('************** Start Scrapping ******** ')
        m_scrapper = MainScrapper(y_links)

        time.sleep(1)
        logging.info('************** Successfully completed! ******** ')

        endtime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        logging.info(f'******** start time:\t{starttime}')
        logging.info(f'******** end time:\t{endtime}')
    except KeyboardInterrupt:
        # quit
        logging.warn("Keyboard Interrupt occured, so quitting.")
        sys.exit(2)
    finally:
        Q.join()
        m_scrapper.kill_threads()
        m_scrapper.save_xls_file()

    