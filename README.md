INSTALLATION :

Install the python 3.6.x version.
pip install -r requirements.txt


config.yaml :

update the config.yaml file with required .yaml file links.
This configuration is used to fetch the bug details.


RUN :

C:\> python  scrapper.py -h
usage: scrapper.py [-h] [-l {DEBUG,WARN,ERROR,INFO}] [-V]

scrapper -- shortdesc

      Created by user_name on 2019-01-26.

    USAGE


optional arguments:
  -h, --help            show this help message and exit
  -l {DEBUG,WARN,ERROR,INFO}, --log {DEBUG,WARN,ERROR,INFO}
                        log level [Default DEBUG], [Options: WARN, ERROR,
                        INFO]
  -V, --version         show program's version number and exit

